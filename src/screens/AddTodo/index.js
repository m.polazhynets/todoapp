import React, { useState, useEffect, useRef } from "react";
import {
  ScrollView,
  View,
  Text,
  Button,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
} from "react-native";
import { TextInput } from "react-native-paper";
import * as ImagePicker from "expo-image-picker";
import { Audio } from "expo-av";
import { useDispatch } from "react-redux";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";

import { addTodo } from "../../store/redux";

const AddTodo = (props) => {
  const {navigation} = props
  const AudioRecorder = useRef(new Audio.Recording());

  const [title, setTitle] = useState();
  const [description, setDescription] = useState();
  const [date, setDate] = useState(moment().toDate());
  const [time, setTime] = useState(moment().toDate());
  const [RecordedURI, SetRecordedURI] = useState("");
  const [AudioPermission, SetAudioPermission] = useState(false);
  const [IsRecording, SetIsRecording] = useState(false);

  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [image, setImage] = useState(null);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  // Initial Load to get the audio permission
  useEffect(() => {
    GetPermission();
  }, []);

  // Function to get the audio permission
  const GetPermission = async () => {
    const getAudioPerm = await Audio.requestPermissionsAsync();
    SetAudioPermission(getAudioPerm.granted);
  };

  // Function to start recording
  const StartRecording = async () => {
    try {
      // Check if user has given the permission to record
      if (AudioPermission === true) {
        try {
          // Prepare the Audio Recorder
          await AudioRecorder.current.prepareToRecordAsync(
            Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY
          );

          // Start recording
          await AudioRecorder.current.startAsync();
          SetIsRecording(true);
        } catch (error) {
          console.log(error);
        }
      } else {
        // If user has not given the permission to record, then ask for permission
        GetPermission();
      }
    } catch (error) {}
  };

  // Function to stop recording
  const StopRecording = async () => {
    try {
      // Stop recording
      await AudioRecorder.current.stopAndUnloadAsync();

      // Get the recorded URI here
      const result = AudioRecorder.current.getURI();
      if (result) SetRecordedURI(result);

      // Reset the Audio Recorder
      AudioRecorder.current = new Audio.Recording();
      SetIsRecording(false);
    } catch (error) {}
  };

  const dispatch = useDispatch();

  function handleSumbit() {
    dispatch(
      addTodo({
        title: title,
        description: description,
        date: moment(date).format("YYYY.MM.DD"),
        time: moment(time).format("HH:mm"),
        image: image,
        audio: RecordedURI,
      })
    );
    setTitle("");
    setDescription("");
    setDate(moment());
    setTime(moment());
    setImage(null);
    SetRecordedURI('');
    navigation.goBack();
  }

  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.content}>
      <View style={styles.item}>
        <TextInput
          label="Title"
          mode={"outlined"}
          value={title}
          onChangeText={setTitle}
        />
      </View>
      <View style={styles.item}>
        <TextInput
          label="Description"
          mode={"outlined"}
          value={description}
          onChangeText={setDescription}
          multiline={true}
        />
      </View>

      <View style={styles.item}>
        <Text style={styles.title}>Choose a date:</Text>
        <TouchableOpacity onPress={() => setShowDatePicker(true)}>
          <Text>{moment(date).format("YYYY.MM.DD")}</Text>
        </TouchableOpacity>
        {showDatePicker && (
          <DateTimePicker
            value={moment(date).toDate()}
            mode={"date"}
            display="default"
            onChange={(event, selectedDate) => {
              setDate(selectedDate);
              setShowDatePicker(false);
            }}
          />
        )}
      </View>

      <View style={styles.item}>
        <Text style={styles.title}>Choose a time:</Text>
        <TouchableOpacity onPress={() => setShowTimePicker(true)}>
          <Text>{moment(time).format("HH:mm")}</Text>
        </TouchableOpacity>
        {showTimePicker && (
          <DateTimePicker
            value={moment(time).toDate()}
            mode={"time"}
            is24Hour={true}
            display="default"
            onChange={(event, selectedTime) => {
              setTime(selectedTime);
              setShowTimePicker(false);
            }}
          />
        )}
      </View>

      <View style={styles.item}>
        <Text style={styles.title}>Attach an image:</Text>
        {!!image ? (
          <Image source={{ uri: image }} style={styles.image} />
        ) : (
          <Button color={"green"} title="Chooce an image" onPress={pickImage} />
        )}
      </View>

      <View style={styles.item}>
        <Text style={styles.title}>Attach an audio:</Text>
        <Button
          title={IsRecording ? "Stop Recording" : "Start Recording"}
          color={IsRecording ? "red" : "green"}
          onPress={IsRecording ? StopRecording : StartRecording}
        />
      </View>

      <View style={styles.item}>
        <Button title="Save" onPress={handleSumbit} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    padding: 15,
  },
  item: {
    marginBottom: 20,
  },
  title: {
    fontWeight: "bold",
    marginBottom: 5,
  },
  image: {
    width: "100%",
    height: 100,
    resizeMode: "contain",
  },
});

export default AddTodo;
