import React, { useRef, useState } from "react";
import { Text, View, Button, StyleSheet, Image } from "react-native";
import { Audio } from "expo-av";
import { Checkbox } from "react-native-paper";

const Card = (props) => {
  const { onToogle, data } = props;
  const AudioPlayer = useRef(new Audio.Sound());
  const [IsPLaying, SetIsPLaying] = useState(false);

  const PlayRecordedAudio = async (RecordedURI) => {
    try {
      // Load the Recorded URI
      await AudioPlayer.current.loadAsync({ uri: RecordedURI }, {}, true);

      // Get Player Status
      const playerStatus = await AudioPlayer.current.getStatusAsync();

      // Play if song is loaded successfully
      if (playerStatus.isLoaded) {
        if (playerStatus.isPlaying === false) {
          AudioPlayer.current.playAsync();
          SetIsPLaying(true);
        }
      }
    } catch (error) {}
  };

  // Function to stop the playing audio
  const StopPlaying = async () => {
    try {
      //Get Player Status
      const playerStatus = await AudioPlayer.current.getStatusAsync();

      // If song is playing then stop it
      if (playerStatus.isLoaded === true)
        await AudioPlayer.current.unloadAsync();

      SetIsPLaying(false);
    } catch (error) {}
  };

  return (
    <View style={styles.item}>
      <View style={styles.itemLeft}>
        <Checkbox
          status={data?.completed ? "checked" : "unchecked"}
          onPress={() => onToogle(data)}
        />
      </View>
      <View style={styles.itemRight}>
        {!!data?.data?.date && !!data?.data?.time && (
          <View style={styles.itemField}>
            <Text>
              {data?.data?.date}
              {", "}
              {data?.data?.time}
            </Text>
          </View>
        )}
        {!!data?.data?.title && (
          <View style={styles.itemField}>
            <Text style={styles.itemTitle}>{data?.data?.title}</Text>
          </View>
        )}
        {!!data?.data?.description && (
          <View style={styles.itemField}>
            <Text style={styles.itemDescription}>
              {data?.data?.description}
            </Text>
          </View>
        )}
        {!!data?.data?.image && (
          <View style={styles.itemField}>
            <Image source={{ uri: data?.data?.image }} style={styles.image} />
          </View>
        )}
        {!!data?.data?.audio && (
          <View style={styles.itemField}>
            <Button
              title={IsPLaying ? "Stop Record" : "Play Record"}
              color={IsPLaying ? "red" : "orange"}
              onPress={() => {
                if (IsPLaying) {
                  StopPlaying();
                } else {
                  PlayRecordedAudio(data?.data?.audio);
                }
              }}
            />
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    flexDirection: "row",
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    paddingVertical: 10,
  },
  itemLeft: {
    width: 40,
  },
  itemRight: {
    flexDirection: "column",
    flex: 1,
  },
  itemField: {
    marginVertical: 5,
  },
  itemTitle: {
    fontWeight: "bold",
    fontSize: 18,
  },
  itemDescription: {
    fontSize: 16,
  },
  image: {
    width: "100%",
    height: 100,
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    marginVertical: 10,
  },
});

export default Card;
