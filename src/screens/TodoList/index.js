import React, { useRef, useState } from "react";
import { Text, View, Button, StyleSheet, FlatList, Image } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { Audio } from "expo-av";
import { Divider } from "react-native-paper";

import Card from "./card";
import { toggleTodo } from "../../store/redux";

const TodoList = (props) => {
  const { navigation } = props;
  const dispatch = useDispatch();
  const todos = useSelector((state) => state.todos);

  const EmptyList = () => (
    <Text style={styles.title}>Start creating a new todo</Text>
  );

  return (
    <View style={styles.content}>
      <View style={styles.buttonWrapper}>
        <Button
          title={"Add task"}
          color={"green"}
          onPress={() => navigation.navigate("AddTodo")}
        />
      </View>
      <Divider style={styles.divider} />
      <Text style={styles.title}>Todo List</Text>
      <FlatList
        data={todos}
        keyExtractor={(_, index) => `t-${index}`}
        renderItem={({ item }) => (
          <Card onToogle={(data) => dispatch(toggleTodo(data))} data={item} />
        )}
        ListEmptyComponent={<EmptyList />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    padding: 15,
  },
  buttonWrapper: {
    marginBottom: 20,
  },
  divider: {
    borderBottomColor: "#aaa",
    borderBottomWidth: 1,
  },
  item: {
    flexDirection: "row",
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    paddingVertical: 10,
  },
  itemLeft: {
    width: 40,
  },
  itemRight: {
    flexDirection: "column",
    flex: 1,
  },
  itemField: {
    marginVertical: 5,
  },
  itemTitle: {
    fontWeight: "bold",
    fontSize: 18,
  },
  itemDescription: {
    fontSize: 16,
  },
  image: {
    width: "100%",
    height: 100,
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    marginVertical: 10,
  },
});

export default TodoList;
